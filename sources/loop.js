define(

    function module( ) {

        'use strict' ;

        var Loop ;

        Loop = function Loop( ) {

            var currentUpdate ;
            var elapsedTime ;
            var initialize ;
            var lastUpdate ;

            Loop.prototype.render = function render( render ) {

                // call user's render callback
                render( ) ;

                // call render on each available frame
                requestAnimationFrame( this.render.bind( this, render ) ) ;
            } ;

            Loop.prototype.update = function update( update ) {

                // define current time update
                currentUpdate = Date.now( ) ;

                // define elapsed time since last update
                elapsedTime += currentUpdate - ( lastUpdate || currentUpdate ) ;

                // call user's update matching framerate and fixing browser time handling
                while ( elapsedTime >= 1000 / this.framerate ) {

                    // define elapsed time since last user's update matching framerate
                    elapsedTime -= 1000 / this.framerate ;

                    // call user's update callback
                    update( ) ;
                }

                // define current time update for next update
                lastUpdate = currentUpdate ;

                // call update matching framerate
                setTimeout( this.update.bind( this, update ), 1000 / this.framerate ) ;
            } ;

            initialize = function initialize( ) {

                this.framerate = 60 ;

                elapsedTime = 0 ;

            // keep the context
            }.bind( this ) ;

            // initialize this instance
            initialize( ) ;
        } ;

        return ( Loop ) ;
    }
) ;
